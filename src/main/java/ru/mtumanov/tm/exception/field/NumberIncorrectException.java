package ru.mtumanov.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("ERROR! Number is incorrect!");
    }

    public NumberIncorrectException(final String value) {
        super("ERROR! Value: \"" + value + "\" is incorrect!");
    }

}
