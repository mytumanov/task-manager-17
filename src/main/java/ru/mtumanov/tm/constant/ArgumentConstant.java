package ru.mtumanov.tm.constant;

public class ArgumentConstant {

    public static final String CMD_HELP = "-h";

    public static final String CMD_VERSION = "-v";

    public static final String CMD_ABOUT = "-a";

    public static final String CMD_INFO = "-i";

    public static final String CMD_COMMANDS = "-cmd";

    public static final String CMD_ARGUMENTS = "-arg";

}
