package ru.mtumanov.tm.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.entity.TaskNotFoundException;
import ru.mtumanov.tm.exception.field.AbstractFieldException;
import ru.mtumanov.tm.exception.field.DescriptionEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.IndexIncorectException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.model.Task;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void add(final Task task) throws AbstractEntityNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task create(final String name, final String description) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task findOneById(final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) throws AbstractFieldException {
        if (index == null || index < 0) throw new IndexIncorectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public void remove(final Task task) throws AbstractEntityNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorectException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorectException();
        if (index > taskRepository.getSize()) throw new IndexOutOfBoundsException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

}
