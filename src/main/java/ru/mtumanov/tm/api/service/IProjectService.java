package ru.mtumanov.tm.api.service;

import java.util.Comparator;
import java.util.List;

import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.field.AbstractFieldException;
import ru.mtumanov.tm.model.Project;

public interface IProjectService {

    void add(Project project) throws AbstractEntityNotFoundException;

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    Project findOneById(String id) throws AbstractFieldException;

    Project findOneByIndex(Integer index) throws AbstractFieldException;

    void remove(Project project) throws AbstractEntityNotFoundException;

    Project removeById(String id) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project removeByIndex(Integer index) throws AbstractFieldException, AbstractEntityNotFoundException;

    void clear();

    Project create(String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project updateById(String id, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project changeProjectStatusById(String id, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

}
