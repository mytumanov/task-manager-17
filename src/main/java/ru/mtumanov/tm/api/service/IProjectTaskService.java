package ru.mtumanov.tm.api.service;

import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.field.AbstractFieldException;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId) throws AbstractFieldException, AbstractEntityNotFoundException;

    void removeProjectById(String projectId) throws AbstractFieldException, AbstractEntityNotFoundException;

    void unbindTaskFromProject(String projectId, String taskId) throws AbstractFieldException, AbstractEntityNotFoundException;

}
